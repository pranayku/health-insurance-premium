package com.health;

import org.junit.Test;

public class JunitTest {

	@Test
	public void test() {
		HeathInsurance obj = new HeathInsurance();
		double premium = 5000.00;

		CandidateBean person = new CandidateBean();
		person.setName("Norman Gomes");
		person.setGender("Male");
		person.setAge(34);
		person.setHypertension(false);
		person.setBloodPressure(false);
		person.setBloodSugar(false);
		person.setOverweight(true);
		person.setSmoking(false);
		person.setAlcohol(true);
		person.setDailyExercise(true);
		person.setDrugs(false);
		System.out
				.println("Test Case executed Sucessfully & premium Amount is = " + obj.businessLogic(person, premium));
	}

}
