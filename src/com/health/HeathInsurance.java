package com.health;

public class HeathInsurance {

	private double premium = 5000.00;

	public static void main(String[] args) {
		HeathInsurance insurance = new HeathInsurance();
		double candidatePreminum = insurance.businessLogic(insurance.inputDetails(), insurance.premium);
		System.out.println("Candidate Preminum ==> " + candidatePreminum);
	}

	public CandidateBean inputDetails() {
		CandidateBean person = new CandidateBean();
		person.setName("Norman Gomes");
		person.setGender("Male");
		person.setAge(34);
		person.setHypertension(false);
		person.setBloodPressure(false);
		person.setBloodSugar(false);
		person.setOverweight(true);
		person.setSmoking(false);
		person.setAlcohol(true);
		person.setDailyExercise(true);
		person.setDrugs(false);
		return person;
	}

	public double businessLogic(CandidateBean person, double premium) {
		double finalPremium = premium;
		for (int personAge = 18; personAge <= person.getAge();) {
			if (personAge <= 25) {
				finalPremium = finalPremium + (finalPremium * 10) / 100;
				personAge = 25;
			}
			if (personAge >= 25 && personAge <= 40) {
				finalPremium = finalPremium + (finalPremium * 10) / 100;
				personAge = personAge + 5;
			}
			if (personAge >= 40 && personAge <= 120) {
				finalPremium = finalPremium + (finalPremium * 20) / 100;
				personAge = personAge + 5;
			}
		}

		if (person.getGender().equalsIgnoreCase("Male")) {
			finalPremium = (finalPremium + (finalPremium * 2) / 100);
		} else if (person.getGender().equalsIgnoreCase("Female")) {
			finalPremium = (finalPremium + (finalPremium * 4) / 100);
		} else if (person.getGender().equalsIgnoreCase("Other")) {
			finalPremium = (finalPremium + (finalPremium * 6) / 100);
		}

		if (person.getHypertension()) {
			finalPremium = (finalPremium + (finalPremium * 1) / 100);
		}
		if (person.getBloodPressure()) {
			finalPremium = (finalPremium + (finalPremium * 1) / 100);
		}
		if (person.getBloodSugar()) {
			finalPremium = (finalPremium + (finalPremium * 1) / 100);
		}
		if (person.getOverweight()) {
			finalPremium = (finalPremium + (finalPremium * 1) / 100);
		}

		if (person.getDailyExercise()) {
			finalPremium = (finalPremium - (finalPremium * 3) / 100);
		}

		if (person.getSmoking()) {
			finalPremium = (finalPremium + (finalPremium * 3) / 100);
		}
		if (person.getAlcohol()) {
			finalPremium = (finalPremium + (finalPremium * 3) / 100);
		}
		if (person.getDrugs()) {
			finalPremium = (finalPremium + (finalPremium * 3) / 100);
		}
		return finalPremium;
	}

}
