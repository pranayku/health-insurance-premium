package com.health;

public class CandidateBean {

	private String name;
	private String gender;
	private int age;
	private Boolean hypertension;
	private Boolean bloodPressure;
	private Boolean bloodSugar;
	private Boolean overweight;
	private Boolean smoking;
	private Boolean alcohol;
	private Boolean dailyExercise;
	private Boolean drugs;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Boolean getHypertension() {
		return hypertension;
	}

	public void setHypertension(Boolean hypertension) {
		this.hypertension = hypertension;
	}

	public Boolean getBloodPressure() {
		return bloodPressure;
	}

	public void setBloodPressure(Boolean bloodPressure) {
		this.bloodPressure = bloodPressure;
	}

	public Boolean getBloodSugar() {
		return bloodSugar;
	}

	public void setBloodSugar(Boolean bloodSugar) {
		this.bloodSugar = bloodSugar;
	}

	public Boolean getOverweight() {
		return overweight;
	}

	public void setOverweight(Boolean overweight) {
		this.overweight = overweight;
	}

	public Boolean getSmoking() {
		return smoking;
	}

	public void setSmoking(Boolean smoking) {
		this.smoking = smoking;
	}

	public Boolean getAlcohol() {
		return alcohol;
	}

	public void setAlcohol(Boolean alcohol) {
		this.alcohol = alcohol;
	}

	public Boolean getDailyExercise() {
		return dailyExercise;
	}

	public void setDailyExercise(Boolean dailyExercise) {
		this.dailyExercise = dailyExercise;
	}

	public Boolean getDrugs() {
		return drugs;
	}

	public void setDrugs(Boolean drugs) {
		this.drugs = drugs;
	}

}
